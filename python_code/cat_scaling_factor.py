import pandas as pd
import numpy as np

networks = pd.read_csv('/home/jliu/projects/v_1_5/resproject_1121_scaling/reference_files/networks_20180701_rweek.txt',
                      sep = "|")
summarziable_networks = networks[(networks.summarize_from_date.isnull()==False)
#          &(pd.to_datetime(networks.summarize_from_date)<=pd.to_datetime('2017-08-28'))
        & ((networks.summarize_to_date.isnull()==True)|
              (pd.to_datetime(networks.summarize_to_date)>=pd.to_datetime('2017-08-28'))
          )]
summarziable_networks=summarziable_networks[summarziable_networks.network_no.isin([431, 5729, 5135,471,6365,6314,6423,434,
                                                                                  5065,5067,5063,5071,5069,322,21,22,23,24,26,413
                                                                                  ])==False]
################ by network scaling ######################

by_net_scaling_factor['from_date'] = np.where(by_net_scaling_factor.mso_no==12,
                                                  '2017-08-28',
                                                  '2018-01-29')
by_net_scaling_factor['to_date'] =''





c_dir = '/home/jliu/projects/v_1_5/resproject_1121_scaling/toup_files/live/'
by_net_scaling1 = pd.read_csv(c_dir + 'live_by_net_mso_scaling_dish_directv_cox_toup_20180710_eff20170828_v2.txt', sep = "|")
by_net_scaling2 = pd.read_csv('/home/jliu/projects/v_1_5/resproject_1121_scaling/toup_files/output_files/live_by_net_mso_scaling_charter_twc_toup_20180712.txt', sep = "|")


default_scaling1 = pd.read_csv(c_dir + 'live_default_mso_scaling_dish_directv_cox_toup_20180710_eff20170828_v2.txt', sep = "|")
default_scaling2 = pd.read_csv('/home/jliu/projects/v_1_5/resproject_1121_scaling/toup_files/output_files/live_default_mso_scaling_charter_twc_toup_20180712.txt', sep = "|")

by_net_scaling = pd.concat([by_net_scaling1, by_net_scaling2])
default_scaling = pd.concat([default_scaling1, default_scaling2])

mso_source_id = {'source_id':[1,2,3,4,5,6], 'mso_no': [1, 12, 114, 102, 4, 104] }
by_net_scaling=pd.merge(by_net_scaling, pd.DataFrame(mso_source_id), on = ['mso_no'],how = 'left')
default_scaling=pd.merge(default_scaling, pd.DataFrame(mso_source_id), on = ['mso_no'],how = 'left')




by_net_scaling.rename(columns = {'network_no':'network_id'}, inplace=True)



by_net_scaling[['source_id', 'network_id', 'scaling_factor', 'from_date', 'to_date']].to_csv('/home/jliu/projects/v_1_5/resproject_1121_scaling/toup_files/by_net_mso_scaling_factor_toup_final_20180712.txt', sep="|", index=False)

default_scaling[['source_id',  'scaling_factor', 'from_date', 'to_date']].to_csv('/home/jliu/projects/v_1_5/resproject_1121_scaling/toup_files/default_mso_scaling_factor_toup_final_20180712.txt', sep="|", index=False)



