import pandas as pd
import numpy as np
import psutil
psutil.virtual_memory()
######### by net files ######
###### step 1 calculate 1stb - 6stb scaling from Dish, DirecTV and ATT fully reporting STBs
# https://jira.comscore.com/browse/RESFACTORS-302
import reg_scaling_calc as cc
# import sql1

dvr_dir = '/data_storage/home/jliu/projects/v_1_5/resproject_1121_scaling/repo_data/DVR_scaling/r2/'


# All network 3 months for denorminator
### note that this file is from DVR h calculation. Please refer to DVR h of how to generate this file.
# dvr_hh_compo = pd.read_csv('/home/pchen/2017/sep2017/dvr_h_r2/sas_output/dvr_hh_compo_sept2017.txt',
dvr_hh_compo = pd.read_csv(dvr_dir + 'dvr_hh_compo_sept2017.txt',
             sep = '|',
             dtype = {0:int,1:str}
               )
dvr_hh_compo.rename(columns = {'num_stbs':'total_num_stbs'},inplace=True)
headend = pd.read_csv('/home/jliu/projects/v_1_5/resproject_1121_scaling/reference_files/headends.txt', sep ="|")
dvr_hh_compo2= pd.merge(dvr_hh_compo, 
                       headend, 
                       on = "headend_no",
                       how = 'left')
dvr_all_net_dish_dtv_cox_month = dvr_hh_compo2.groupby(['mso_no', 
                                                       "num_reporting_stbs", 
                                                       "total_num_stbs"]
                                                      ,as_index=False)['hh_id'].count()
dvr_all_net_dish_dtv_cox_month.rename (columns = {'hh_id':'hhs'},inplace=True)
dvr_all_net_dish_dtv_cox_month.head()

# dvr_all_net_dish_dtv_cox_month[dvr_all_net_dish_dtv_cox_month.mso_no ==102].to_csv(dvr_dir +'check.txt', sep="|",index=False)
# By network nominator
cols1 = [  'mso_no',
        'broadcast_day',
        'num_reporting_stbs',
        'num_stbs',
        'network_no',
        'dvr_hours_bsd',
        'dvr_hours_b24',
        'dvr_hours_b48',
        'dvr_hours_b72',
        'dvr_hours_b168',
        'dvr_hours_b360',
        'dvr_hhs_bsd',
        'dvr_hhs_b24',
        'dvr_hhs_b48',
        'dvr_hhs_b72',
        'dvr_hhs_b168',
        'dvr_hhs_b360',
        'dvr_num_hours' ,
        'dvr_num_hhs']
######## data are pulling from Live REPO ##############
dvr_by_net_scaling_vao_day_p1 = pd.read_csv(dvr_dir + 'dvr_by_net_scaling_vao_national_v2_linear_national.txt',
             sep = '|',header = None,names = cols1)
#### exception data set 1: new unified networks
#  (431,5729, 5135,471,6365,6314,6423,434)
dvr_by_net_scaling_vao_day_p2 = pd.read_csv(dvr_dir + 'dvr_by_net_scaling_vao_national_new_unifieds_linear_national.txt',
             sep = '|',header = None,names = cols1)
# 7274
dvr_by_net_scaling_vao_day_p3 = pd.read_csv(dvr_dir + 'dvr_by_net_scaling_vao_national_eleven_linear_directv.txt',
             sep = '|',header = None,names = cols1)

dvr_by_net_scaling_vao_day=pd.concat([
    dvr_by_net_scaling_vao_day_p1[((dvr_by_net_scaling_vao_day_p1.network_no.isin([431, 5729, 5135,471,6365,6314,6423,434]))
                              | ((dvr_by_net_scaling_vao_day_p1.network_no==7274) 
                                 & (dvr_by_net_scaling_vao_day_p1.mso_no == 102 )))==False
                              ],
    dvr_by_net_scaling_vao_day_p2,
    dvr_by_net_scaling_vao_day_p3   
    ])
dvr_by_net_scaling_vao_day.rename (columns = {'dvr_num_hours':'hours',
                                            'dvr_num_hhs':'hhs',
                                             'num_stbs':'total_num_stbs'},inplace=True)
dvr_by_net_scaling_vao_day['duration']='day'
dvr_by_net_scaling_vao_day['for_default']=1

dvr_by_net_scaling_vao_day_clean = cc.inital_clean(dvr_by_net_scaling_vao_day)
dvr_all_net_dish_dtv_cox_month_clean = cc.inital_clean(dvr_all_net_dish_dtv_cox_month)

#### by network norminator
net_duration_hhs =dvr_by_net_scaling_vao_day_clean.groupby(['duration',
                                                              'network_no',
                                                              'mso_no', 
                                                              'num_reporting_stbs', 
                                                              'total_num_stbs'], 
                                      as_index=False).agg({"hhs": "sum"})
### dish direcTV denominator
allnet_month_hhs= dvr_all_net_dish_dtv_cox_month_clean.groupby(['mso_no', 'num_reporting_stbs', 'total_num_stbs'], 
                                                as_index=False).agg({"hhs": "sum"})
##### default
#### nominator for default
default_norm = dvr_by_net_scaling_vao_day_clean[dvr_by_net_scaling_vao_day_clean['for_default']==1].groupby([
                                                                                                'duration',
                                                                                                'mso_no', 
                                                                                                'num_reporting_stbs', 
                                                                                                'total_num_stbs'], 
                                      as_index=False).agg({"hhs": "sum"})

dvr_dish_directv_scaling = cc.scaling_calc_reg (
                      norm = net_duration_hhs, 
                      denorm = allnet_month_hhs, 
                      merge_key =['mso_no', 'num_reporting_stbs', 'total_num_stbs'])

default_norm['network_no'] = 99999
dvr_default_dish_directv_scaling = cc.scaling_calc_reg (
                      norm = default_norm[default_norm.mso_no.isin([102, 4])].copy(), 
                      denorm = allnet_month_hhs, 
                      merge_key =['mso_no', 'num_reporting_stbs', 'total_num_stbs'])




networks = pd.read_csv('/home/jliu/projects/v_1_5/resproject_1121_scaling/reference_files/networks_20180701_rweek.txt',
                      sep = "|")
summarziable_networks = networks[(networks.summarize_from_date.isnull()==False)
#          &(pd.to_datetime(networks.summarize_from_date)<=pd.to_datetime('2017-08-28'))
        & ((networks.summarize_to_date.isnull()==True)|
              (pd.to_datetime(networks.summarize_to_date)>=pd.to_datetime('2017-08-28'))
          )]
summarziable_networks=summarziable_networks[summarziable_networks.network_no.isin([431, 5729, 5135,471,6365,6314,6423,434,
                                                                                  5065,5067,5063,5071,5069,322,21,22,23,24,26,413
                                                                                  ])==False]



mso_source_id = {'source_id':[1,2,3,4,5,6], 'mso_no': [1, 12, 114, 102, 4, 104] }
dvr_dish_directv_scaling=pd.merge(dvr_dish_directv_scaling, pd.DataFrame(mso_source_id), on = ['mso_no'],how = 'left')


out_dir = '/home/jliu/projects/v_1_5/resproject_1121_scaling/toup_files/'
################ by network scaling ######################
dvr_by_net_mso_scaling_toup =dvr_dish_directv_scaling.loc[(dvr_dish_directv_scaling.hhs_net_full >=500)
                                                    & (dvr_dish_directv_scaling.network_no.isin(summarziable_networks.network_no)),
                                                       ['mso_no','network_no', 'scaling_factor']]
dvr_by_net_mso_scaling_toup['from_date'] ='2017-08-28'
dvr_by_net_mso_scaling_toup['to_date'] =''
dvr_by_net_mso_scaling_toup.to_csv(out_dir + 'dvr_by_net_mso_scaling_factor_toup_final_20180712_v2.txt',
                              sep="|", index=False)
################ default scaling ######################
dvr_default_mso_scaling_toup =dvr_default_dish_directv_scaling.loc[:, ['mso_no','scaling_factor']]
dvr_default_mso_scaling_toup['from_date'] ='2017-08-28'
dvr_default_mso_scaling_toup['to_date'] =''
dvr_default_mso_scaling_toup.to_csv(out_dir + 'dvr_default_mso_scaling_factor_toup_final_20180712_v2.txt',
                              sep="|", index=False)



################ ################ ################ ################ ################ ################ ################ ################ ################ 
# for matrix spreadsheet
# net_duration_hhs
# paste to net_HH_matrix
net_HH_matrix = pd.pivot_table(net_duration_hhs, values='hhs_net', index=['duration','mso_no', 'network_no', 'total_num_stbs'],
                  columns=['num_reporting_stbs'], aggfunc=np.sum).reset_index()

net_HH_matrix['key'] =  net_HH_matrix["duration"].map(str) + '|' +                         net_HH_matrix["mso_no"].map(str) + '|' +                         net_HH_matrix["network_no"].map(str) + '|' +                         net_HH_matrix["total_num_stbs"].map(str) 

net_HH_matrix.rename(columns = {1:'nuM_rep_stbs1',
                                    2:'nuM_rep_stbs2',
                                    3:'nuM_rep_stbs3',
                                    4:'nuM_rep_stbs4',
                                    5:'nuM_rep_stbs5',
                                    6:'nuM_rep_stbs6'}, inplace=True)
net_HH_matrix[['duration', 'mso_no', 'network_no', 'total_num_stbs', 'key',
              'nuM_rep_stbs1', 'nuM_rep_stbs2', 'nuM_rep_stbs3', 
              'nuM_rep_stbs4', 'nuM_rep_stbs5', 'nuM_rep_stbs6']].to_csv('net_HH_matrix.txt',sep="|",index=False)




# for matrix spreadsheet
# paste to all_net_HH_matrix

all_net_HH_matrix =pd.pivot_table(allnet_month_hhs, values='hhs_all', index=['mso_no',  'total_num_stbs'],
                  columns=['num_reporting_stbs'], aggfunc=np.sum).reset_index()

all_net_HH_matrix['key'] = all_net_HH_matrix["mso_no"].map(str) + '|'+all_net_HH_matrix["total_num_stbs"].map(str) 

all_net_HH_matrix.rename(columns = {1:'nuM_rep_stbs1',
                                    2:'nuM_rep_stbs2',
                                    3:'nuM_rep_stbs3',
                                    4:'nuM_rep_stbs4',
                                    5:'nuM_rep_stbs5',
                                    6:'nuM_rep_stbs6'}, inplace=True)


all_net_HH_matrix[['mso_no', 'total_num_stbs', 'key',
              'nuM_rep_stbs1', 'nuM_rep_stbs2', 'nuM_rep_stbs3', 
              'nuM_rep_stbs4', 'nuM_rep_stbs5', 'nuM_rep_stbs6']].to_csv('all_net_HH_matrix.txt',sep="|",index=False)


# In[115]:


dvr_scaling_tmp1  = pd.merge (summarziable_networks,
                    mso_by_net_scaling_factor2[['duration',
                            'mso_no','network_no',
                            'final_scaling_factor']],
                           on = ['network_no'],
                           how = 'outer'                          
                          )


# In[131]:


to_Check = dvr_scaling_tmp1[((dvr_scaling_tmp1.summarize_from_date.isnull() )
                |(dvr_scaling_tmp1.final_scaling_factor.isnull() & (pd.to_datetime(dvr_scaling_tmp1.summarize_from_date)<pd.to_datetime('2018-01-01')))
                 )]
to_Check


# In[173]:



qalpha = "select network_no, min(month), sum(num_hours) from views_by_headend_network_month left join headends using(headend_no)         where month >= '2017-01-28' and mso_no in (102,4) group by 1"
v_net = sql1.q(qalpha)
v_net[v_net.network_no.isin(to_Check.network_no)]


dvr_dir = '/data_storage/home/jliu/projects/v_1_5/scaling/sep2017/DVR_scaling/'
import sql1
qalpha = "select network_no, min(month), max(month), sum(num_hours) from views_by_headend_network_month left join headends using(headend_no)         where month >= '2017-01-28' and mso_no in (102,4) and network_no in (5729, 431,12274) group by 1"
v_net = sql1.q(qalpha)


# In[179]:


v_net


# In[52]:


mso_default_scaling_factor


# In[60]:


mso_by_net_scaling_factor2[mso_by_net_scaling_factor2.network_no==32]

