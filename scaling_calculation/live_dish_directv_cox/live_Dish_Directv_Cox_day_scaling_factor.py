# Files needed for H/scaling calculation

# 1. HH summary files (this will be summarized by tech in v1.5/v2),
#     including information of mso_no, HH_id, num_reporting_stbs, total_num_stbs

# Filters: HHs need to have view on both the first and third month
#         Networks need to have more or equal than 500 fully reporting HHs. In scaling factor calc, this was the 
#         sum of daily HHs count, while H factor is the unique HHs across 3 months. This discrency is due to extra
#         data needed if want to use the same unique HHs for scaling. Scaling factor is pulled by day. 


import pandas as pd
import numpy as np
import psutil
psutil.virtual_memory()
### import the function for scaling calculation  ### 

import reg_scaling_calc as cc
# import sql1

def create_input_data(input_file, 
                      col_names ,
                      assign_duration,
                      used_in_default_calc,
                      assign_network = None
                     ): 
    out_data = pd.read_csv(input_file, sep='|',header=None, names = col_names)
    out_data['duration'] = assign_duration
    if assign_network == None:
        out_data['network_no'] =out_data['network_no']
    else:
        or_net_group_char = list(map(str, assign_network))
        or_net_group_char.append('or')
        or_net_group_char.insert(0, 'net')
        or_net_group_comb = '_'.join(or_net_group_char)
        out_data['network_no'] = or_net_group_comb        
    out_data['for_default']=used_in_default_calc
    return out_data


#### set the input directory
c_dir = '/home/jliu/projects/v_1_5/resproject_1121_scaling/repo_data/dish_directv_cox/'
######### by net files ######
##############################################################################################################################
######### by net files ######
by_net_dish_dtv_cox_day_p1 = create_input_data(
    input_file = c_dir+'by_net_h_vao_ddfp_day_linear_national.txt',
    col_names = ["day","mso_no", "num_reporting_stbs", "total_num_stbs",'network_no', "hours", "hhs"],
    assign_duration = 'day',
    used_in_default_calc =1)
#  (431, 5729, 5135,471,6365,6314,6423,434)
by_net_dish_dtv_cox_day_p2 = create_input_data(
    input_file = c_dir+'by_net_scaling_vao_national_new_unifieds_linear_national.txt',
    col_names = ["mso_no","day", "num_reporting_stbs", "total_num_stbs",'network_no', "hours", "hhs"],
    assign_duration = 'day',
    used_in_default_calc =1)
# 7274
by_net_dish_dtv_cox_day_p3 = create_input_data(
    input_file = c_dir+'by_net_scaling_vao_national_eleven_linear_directv.txt',
    col_names = ["mso_no","day", "num_reporting_stbs", "total_num_stbs",'network_no', "hours", "hhs"],
    assign_duration = 'day',
    used_in_default_calc =1)
exception_net = [431,5729,5135,471,6365,6314,6423,434]

by_net_dish_dtv_cox_duration=pd.concat([ 
                                by_net_dish_dtv_cox_day_p1[((by_net_dish_dtv_cox_day_p1.network_no.isin(exception_net))
                              | ((by_net_dish_dtv_cox_day_p1.network_no==7274) 
                                 & (by_net_dish_dtv_cox_day_p1.mso_no == 102 )))==False
                              ],
                            by_net_dish_dtv_cox_day_p2,
                            by_net_dish_dtv_cox_day_p3])
##################################### input for Dish and direcTV denominator ###############################################
all_net_dish_dtv_cox_month = pd.read_csv(c_dir +'all_net_3month_unique_linear_national.txt', 
                                          sep='|', header=None,
                                      names = ["mso_no", "num_reporting_stbs", "total_num_stbs", "hours", "hhs"])
#### input for Cox denominator ####
by_net_dish_dtv_cox_3month_p1 = pd.read_csv(c_dir +'by_net_h_vao_ddfp_3month_linear_national.txt', 
                                       sep='|', header=None,
                                      names = ["mso_no", "num_reporting_stbs", "total_num_stbs",
                                               "network_no",  "hours", "hhs"])
by_net_dish_dtv_cox_3month_p1['for_default']=1
new_unified_nets = [431, 5729, 5135,471,6365,6314,6423,434]
by_net_dish_dtv_cox_3month_new_unified = pd.read_csv(c_dir +'by_net_h_vao_ddfp_3month_new_unifieds_linear_national.txt', 
                                       sep='|', header=None,
                                      names = ["mso_no", "num_reporting_stbs", "total_num_stbs",
                                               "network_no",  "hours", "hhs"])
by_net_dish_dtv_cox_3month_new_unified['for_default']=1
by_net_dish_dtv_cox_3month=pd.concat([by_net_dish_dtv_cox_3month_p1[by_net_dish_dtv_cox_3month_p1.network_no.isin(new_unified_nets)==False], 
                                      by_net_dish_dtv_cox_3month_new_unified ])

all_net_3m_denorm_clean  = cc.inital_clean(all_net_dish_dtv_cox_month)
by_net_3m_denorm_clean   = cc.inital_clean(by_net_dish_dtv_cox_3month)
by_net_dish_dtv_cox_duration_norm_clean = cc.inital_clean(by_net_dish_dtv_cox_duration)


### by network norminator
net_duration_hhs =by_net_dish_dtv_cox_duration_norm_clean.groupby(['duration',
                                                              'network_no',
                                                              'mso_no', 
                                                              'num_reporting_stbs', 
                                                              'total_num_stbs'], as_index=False).agg({"hhs": "sum"})

### dish direcTV denominator
allnet_month_hhs= all_net_3m_denorm_clean.groupby(['mso_no', 'num_reporting_stbs', 'total_num_stbs'], as_index=False).agg({"hhs": "sum"})

# Cox_denominator
net_3month_hhs  = by_net_3m_denorm_clean.groupby(['mso_no', 
                                                          'network_no',
                                                          'num_reporting_stbs', 
                                                          'total_num_stbs'], as_index=False).agg({"hhs": "sum"})

##### default
#### nominator for default
default_norm = by_net_dish_dtv_cox_duration_norm_clean[by_net_dish_dtv_cox_duration_norm_clean['for_default']==1].groupby([
                                                                                                'duration',
                                                                                                'mso_no', 
                                                                                                'num_reporting_stbs', 
                                                                                                'total_num_stbs'],  as_index=False).agg({"hhs": "sum"})


# Cox_denominator_for_default
defualt_cox_denorm =by_net_3m_denorm_clean[by_net_3m_denorm_clean['for_default']==1].groupby(['mso_no', 
                                                                          'num_reporting_stbs', 
                                                                           'total_num_stbs'], as_index=False).agg({"hhs": "sum"})
dish_directv_scaling = cc.scaling_calc_reg (
                      norm = net_duration_hhs[net_duration_hhs.mso_no.isin([102, 4])].copy(), 
                      denorm = allnet_month_hhs, 
                      merge_key =['mso_no', 'num_reporting_stbs', 'total_num_stbs'])

cox_scaling = cc.scaling_calc_reg (
                      norm      = net_duration_hhs[net_duration_hhs.mso_no.isin([114])].copy(), 
                      denorm    = net_3month_hhs, 
                      merge_key = ['mso_no', 'network_no', 'num_reporting_stbs', 'total_num_stbs'])


default_norm['network_no'] = 99999
defualt_cox_denorm['network_no'] = 99999 

default_dish_directv_scaling = cc.scaling_calc_reg (
                      norm = default_norm[default_norm.mso_no.isin([102, 4])].copy(), 
                      denorm = allnet_month_hhs, 
                      merge_key =['mso_no', 'num_reporting_stbs', 'total_num_stbs'])

default_cox_scaling = cc.scaling_calc_reg (
                      norm      = default_norm[default_norm.mso_no.isin([114])].copy(), 
                      denorm    = defualt_cox_denorm, 
                      merge_key = ['mso_no', 'network_no', 'num_reporting_stbs', 'total_num_stbs'])

dish_directv_cox_scaling_factor = pd.concat([dish_directv_scaling, cox_scaling])
default_dish_directv_cox_scaling_factor = pd.concat([default_dish_directv_scaling, default_cox_scaling])

############# subset to summarzable networks
networks = pd.read_csv('/home/jliu/projects/v_1_5/resproject_1121_scaling/reference_files/networks_20180701_rweek.txt',
                      sep = "|")
summarziable_networks = networks[(networks.summarize_from_date.isnull()==False)
#          &(pd.to_datetime(networks.summarize_from_date)<=pd.to_datetime('2017-08-28'))
        & ((networks.summarize_to_date.isnull()==True)|
              (pd.to_datetime(networks.summarize_to_date)>=pd.to_datetime('2017-08-28'))
          )]
summarziable_networks=summarziable_networks[summarziable_networks.network_no.isin([431, 5729, 5135,471,6365,6314,6423,434,
                                                                                  5065,5067,5063,5071,5069,322,21,22,23,24,26,413
                                                                                  ])==False]

out_dir = '/home/jliu/projects/v_1_5/resproject_1121_scaling/toup_files/output_files/'
################ by network scaling ######################
by_net_mso_scaling_toup =dish_directv_cox_scaling_factor.loc[(dish_directv_cox_scaling_factor.hhs_net_full >=500)
                                                    & (dish_directv_cox_scaling_factor.network_no.isin(summarziable_networks.network_no)),
                                                       ['mso_no','network_no', 'scaling_factor']]
by_net_mso_scaling_toup['from_date'] ='2017-08-28'
by_net_mso_scaling_toup['to_date'] =''
by_net_mso_scaling_toup.to_csv(out_dir + 'live_by_net_mso_scaling_dish_directv_cox_toup_20180713_v2.txt',
                              sep="|", index=False)
################ default scaling ######################
default_mso_scaling_toup =default_dish_directv_cox_scaling_factor.loc[:,['mso_no','scaling_factor']]
default_mso_scaling_toup['from_date'] ='2017-08-28'
default_mso_scaling_toup['to_date'] =''
default_mso_scaling_toup.to_csv(out_dir + '/live_default_mso_scaling_dish_directv_cox_toup_20180713_v2.txt',
                              sep="|", index=False)
################################################################################################################################################################################################################################
# for matrix spreadsheet
# net_duration_hhs
# paste to net_HH_matrix
net_HH_matrix = pd.pivot_table(net_duration_hhs, values='hhs_net', index=['duration','mso_no', 'network_no', 'total_num_stbs'],
                  columns=['num_reporting_stbs'], aggfunc=np.sum).reset_index()

net_HH_matrix['key'] =  net_HH_matrix["duration"].map(str) + '|' +                         net_HH_matrix["mso_no"].map(str) + '|' +                         net_HH_matrix["network_no"].map(str) + '|' +                         net_HH_matrix["total_num_stbs"].map(str) 

net_HH_matrix.rename(columns = {1:'nuM_rep_stbs1',
                                    2:'nuM_rep_stbs2',
                                    3:'nuM_rep_stbs3',
                                    4:'nuM_rep_stbs4',
                                    5:'nuM_rep_stbs5',
                                    6:'nuM_rep_stbs6'}, inplace=True)
net_HH_matrix[['duration', 'mso_no', 'network_no', 'total_num_stbs', 'key',
              'nuM_rep_stbs1', 'nuM_rep_stbs2', 'nuM_rep_stbs3', 
              'nuM_rep_stbs4', 'nuM_rep_stbs5', 'nuM_rep_stbs6']].to_csv(out_dir+'net_HH_matrix.txt',sep="|",index=False)

# for matrix spreadsheet
# paste to all_net_HH_matrix

all_net_HH_matrix =pd.pivot_table(allnet_month_hhs, values='hhs_all', index=['mso_no',  'total_num_stbs'],
                  columns=['num_reporting_stbs'], aggfunc=np.sum).reset_index()

all_net_HH_matrix['key'] = all_net_HH_matrix["mso_no"].map(str) + '|'+all_net_HH_matrix["total_num_stbs"].map(str) 

all_net_HH_matrix.rename(columns = {1:'nuM_rep_stbs1',
                                    2:'nuM_rep_stbs2',
                                    3:'nuM_rep_stbs3',
                                    4:'nuM_rep_stbs4',
                                    5:'nuM_rep_stbs5',
                                    6:'nuM_rep_stbs6'}, inplace=True)


all_net_HH_matrix[['mso_no', 'total_num_stbs', 'key',
              'nuM_rep_stbs1', 'nuM_rep_stbs2', 'nuM_rep_stbs3', 
              'nuM_rep_stbs4', 'nuM_rep_stbs5', 'nuM_rep_stbs6']].to_csv(out_dir+'all_net_HH_matrix.txt',sep="|",index=False)


# for matrix spreadsheet
# paste to net_3mHH_matrix_cox
net_3mHH_matrix_cox = pd.pivot_table(net_3month_hhs, values='hhs_3month_net', index=['mso_no', 'network_no', 'total_num_stbs'],
                  columns=['num_reporting_stbs'], aggfunc=np.sum).reset_index()

net_3mHH_matrix_cox['key'] = net_3mHH_matrix_cox["mso_no"].map(str) + '|' +                             net_3mHH_matrix_cox["network_no"].map(str) + '|' +                             net_3mHH_matrix_cox["total_num_stbs"].map(str) 

net_3mHH_matrix_cox.rename(columns = {1:'nuM_rep_stbs1',
                                    2:'nuM_rep_stbs2',
                                    3:'nuM_rep_stbs3',
                                    4:'nuM_rep_stbs4',
                                    5:'nuM_rep_stbs5',
                                    6:'nuM_rep_stbs6'}, inplace=True)

net_3mHH_matrix_cox[[ 'mso_no', 'network_no', 'total_num_stbs', 'key',
              'nuM_rep_stbs1', 'nuM_rep_stbs2', 'nuM_rep_stbs3', 
              'nuM_rep_stbs4', 'nuM_rep_stbs5', 'nuM_rep_stbs6']].to_csv('net_3mHH_matrix_cox.txt',sep="|",index=False)

