import pandas as pd
import os
import numpy as np
from gzip import GzipFile
import io
import gc
import sys
from zipfile import ZipFile
# use Dish DirecTV and ATT fully reporting HHs to caculate h assuming partial reporitng
day_columns = ['mso_no', 'broadcast_day','network_no','num_stbs_in_hh',
             'num_hhs','num_hhs_stb1','num_hhs_stb2','num_hhs_stb3','num_hhs_stb4',
             'num_hhs_stb5','num_hhs_stb6']
########## TWC day ###############
twc_day_p1_f='/home/jliu/projects/v_1_5/resproject_1121_scaling/repo_data/twc_scaling/by_net_h_for_twc_national_scale1_linear_national.txt'
twc_day_p2_f='/home/jliu/projects/v_1_5/resproject_1121_scaling/repo_data/twc_scaling/by_net_h_for_twc_national_scale2_linear_national.txt'
twc_day_p3_f='/home/jliu/projects/v_1_5/resproject_1121_scaling/repo_data/twc_scaling/by_net_h_for_twc_national_scale3_linear_national.txt'

twc_day_p1_att_f='/home/jliu/projects/v_1_5/resproject_1121_scaling/repo_data/twc_scaling/by_net_h_for_twc_national_scale1_att_linear_national.txt'
twc_day_p2_att_f='/home/jliu/projects/v_1_5/resproject_1121_scaling/repo_data/twc_scaling/by_net_h_for_twc_national_scale2_att_linear_national.txt'
twc_day_p3_att_f='/home/jliu/projects/v_1_5/resproject_1121_scaling/repo_data/twc_scaling/by_net_h_for_twc_national_scale3_att_linear_national.txt'

by_net_scaling_for_twc_day_p1 = pd.read_csv(twc_day_p1_f,  sep='|',header = None,names = day_columns)
by_net_scaling_for_twc_day_p2 = pd.read_csv(twc_day_p2_f,  sep='|',header = None,names = day_columns)
by_net_scaling_for_twc_day_p3 = pd.read_csv(twc_day_p3_f,  sep='|',header = None,names = day_columns)

by_net_scaling_for_twc_day_att_p1 = pd.read_csv(twc_day_p1_att_f,  sep='|',header = None,names = day_columns)
by_net_scaling_for_twc_day_att_p2 = pd.read_csv(twc_day_p2_att_f,  sep='|',header = None,names = day_columns)
by_net_scaling_for_twc_day_att_p3 = pd.read_csv(twc_day_p3_att_f,  sep='|',header = None,names = day_columns)

by_net_scaling_for_twc_day=pd.concat([by_net_scaling_for_twc_day_p1[by_net_scaling_for_twc_day_p1['mso_no']!=1],
                                      by_net_scaling_for_twc_day_p2[by_net_scaling_for_twc_day_p2['mso_no']!=1],
                                      by_net_scaling_for_twc_day_p3[by_net_scaling_for_twc_day_p3['mso_no']!=1],
                                      by_net_scaling_for_twc_day_att_p1[by_net_scaling_for_twc_day_att_p1['mso_no']==1],
                                      by_net_scaling_for_twc_day_att_p2[by_net_scaling_for_twc_day_att_p2['mso_no']==1],
                                      by_net_scaling_for_twc_day_att_p3[by_net_scaling_for_twc_day_att_p3['mso_no']==1],
                                     ])        
by_net_scaling_for_twc_day['for_mso_no']=104
by_net_scaling_for_twc_day['duration']='day'

########## charter day ###############
charter_day_p1_f='/home/jliu/projects/v_1_5/resproject_1121_scaling/repo_data/charter_scaling/by_net_scaling_for_charter_day_p1_linear_national.txt'
charter_day_p2_f='/home/jliu/projects/v_1_5/resproject_1121_scaling/repo_data/charter_scaling/by_net_scaling_for_charter_day_p2_linear_national.txt'
charter_day_p3_f='/home/jliu/projects/v_1_5/resproject_1121_scaling/repo_data/charter_scaling/by_net_scaling_for_charter_day_p3_linear_national.txt'

charter_day_dark_f='/home/jliu/projects/v_1_5/resproject_1121_scaling/repo_data/charter_scaling/by_net_scaling_for_charter_day_darkstations_linear_national.txt'
charter_day_new_unified_f='/home/jliu/projects/v_1_5/resproject_1121_scaling/repo_data/charter_scaling/by_net_scaling_for_charter_day_new_unifieds_linear_national.txt'

by_net_scaling_for_charter_day_p1 = pd.read_csv(charter_day_p1_f,  sep='|',header = None,names = day_columns)
by_net_scaling_for_charter_day_p2 = pd.read_csv(charter_day_p2_f,  sep='|',header = None,names = day_columns)
by_net_scaling_for_charter_day_p3 = pd.read_csv(charter_day_p3_f,  sep='|',header = None,names = day_columns)
exception_net = [431,5729,5135,471,6365,6314,6423,434]
by_net_scaling_for_charter_day_new_unified = pd.read_csv(charter_day_new_unified_f,  sep='|',header = None,names = day_columns)
by_net_scaling_for_charter_day=pd.concat([by_net_scaling_for_charter_day_p1[by_net_scaling_for_charter_day_p1.network_no.isin(exception_net)==False],\
                                          by_net_scaling_for_charter_day_p2[by_net_scaling_for_charter_day_p2.network_no.isin(exception_net)==False],\
                                          by_net_scaling_for_charter_day_p3[by_net_scaling_for_charter_day_p3.network_no.isin(exception_net)==False],\
                                          by_net_scaling_for_charter_day_new_unified\
                                         ])       
by_net_scaling_for_charter_day['for_mso_no']=12
by_net_scaling_for_charter_day['duration']='day'
fully_HHs_hours_stb_order = pd.concat([by_net_scaling_for_twc_day,
                                       by_net_scaling_for_charter_day,
                                                    ] )
# weight above aculated h by charter pct HH per reporting STBs
hh_stb_col = ['mso_no','tv_market_no','headend_no','hh_id', 'repo_stb_id', 
                           'num_stbs', 'hours_m1', 'hours_m2', 'hours_m3']
twc_hh_stb_f_zip = ZipFile('/home/jwong/work/PROJECTS/2017_TWC_H/Repo_pull/TWC_repo_03/repo_hh_compo_resfactors356_twc_standby.zip')
hh_stb_f_zip = ZipFile('/data_storage/work/cleffers/sept2017/H_Factors/Round2/repo_hh_compo_resfactors148_linear_national.zip')
# for text_file in hh_stb_f_zip.infolist():
#        print (text_file.filename)
twc_hh_stb_f = twc_hh_stb_f_zip.open('repo_hh_compo_resfactors356_twc_standby.txt')
charter_hh_stb_f = hh_stb_f_zip.open('repo_hh_compo_resfactors148_charter_charter.txt')
repo_hh_stb_twc = pd.read_csv(twc_hh_stb_f, 
                              sep='|',
                              dtype={0: int, 1: int, 2: int, 3: str, 4: str},
                              header = None,
                              names = hh_stb_col )

repo_hh_stb_charter = pd.read_csv(charter_hh_stb_f, 
                              sep='|',
                              dtype={0: int, 1: int, 2: int, 3: str, 4: str},
                              header = None,
                              names = hh_stb_col )
repo_hh_stb_twc_charter=pd.concat([repo_hh_stb_twc[repo_hh_stb_twc.mso_no==104],
                          repo_hh_stb_charter[repo_hh_stb_charter.mso_no==12]])

repo_hh_stb_twc_charter['num_reporting_stbs'] = 1
repo_hh_twc_charter = repo_hh_stb_twc_charter.groupby(['mso_no','hh_id'],
                                                    as_index = False).aggregate({'hours_m1':'sum',
                                                                            'hours_m2':'sum',
                                                                            'hours_m3':'sum',
                                                                            'num_reporting_stbs':'count'})
# subset to HHs with both first and third month viewership
repo_hh_pick = repo_hh_twc_charter[(repo_hh_twc_charter.loc[:,'hours_m1'] > 0) 
                                   & (repo_hh_twc_charter.loc[:,'hours_m3'] > 0)].copy()

del(repo_hh_stb_twc_charter)
del(repo_hh_stb_twc)
del(repo_hh_stb_charter)
del(repo_hh_twc_charter)
gc.collect()
psutil.virtual_memory()
repo_hh_pick=repo_hh_pick.reset_index()
# cap num_reporting_stbs 5
repo_hh_pick.loc[:,('calc_num_reporting_stbs')] = np.minimum(5,repo_hh_pick.loc[:,('num_reporting_stbs')] )

num_hh_per_rep_stb = repo_hh_pick.groupby(['mso_no',
                                           'calc_num_reporting_stbs'],
                                           as_index= False).aggregate({'hh_id':'count'})

num_hh_per_rep_stb.rename(columns={'hh_id':'num_hhs'}, inplace=True)
num_hh_per_rep_stb["total_hhs"]  = num_hh_per_rep_stb.groupby('mso_no',as_index=False)["num_hhs"].transform('sum')

num_hh_per_rep_stb['pct_hh'] = num_hh_per_rep_stb.num_hhs/num_hh_per_rep_stb.total_hhs

num_hh_per_rep_stb.rename(columns={'mso_no':'for_mso_no'}, inplace=True)
num_hh_per_rep_stb_w=num_hh_per_rep_stb.pivot(index='for_mso_no', 
                                              columns='calc_num_reporting_stbs', 
                                              values='pct_hh')

num_hh_per_rep_stb_w.columns = ['pct_hh_stb1','pct_hh_stb2','pct_hh_stb3','pct_hh_stb4','pct_hh_stb5']

fully_HHs_hours_stb_order.loc[:,'calc_num_stbs'] = np.minimum(6,fully_HHs_hours_stb_order.loc[:,('num_stbs_in_hh')] )
fully_HHs_hours_stb_order_agg_temp = fully_HHs_hours_stb_order.groupby(['duration',
                                                                   'for_mso_no',
                                                                   'mso_no',
                                                                   'network_no'],
                                                                  as_index=False).aggregate({'num_hhs':'sum',
                                                                                            'num_hhs_stb1':'sum',
                                                                                            'num_hhs_stb2':'sum',
                                                                                            'num_hhs_stb3':'sum',
                                                                                            'num_hhs_stb4':'sum',
                                                                                            'num_hhs_stb5':'sum',
                                                                                            'num_hhs_stb6':'sum'})

fully_HHs_hours_stb_order_agg_all_net = fully_HHs_hours_stb_order.groupby(['duration',
                                                                   'for_mso_no',
                                                                   'mso_no',],
                                                                  as_index=False).aggregate({'num_hhs':'sum',
                                                                                            'num_hhs_stb1':'sum',
                                                                                            'num_hhs_stb2':'sum',
                                                                                            'num_hhs_stb3':'sum',
                                                                                            'num_hhs_stb4':'sum',
                                                                                            'num_hhs_stb5':'sum',
                                                                                            'num_hhs_stb6':'sum'})
fully_HHs_hours_stb_order_agg_all_net['network_no'] =99999

fully_HHs_hours_stb_order_agg=pd.concat([fully_HHs_hours_stb_order_agg_temp,fully_HHs_hours_stb_order_agg_all_net])

fully_HHs_hours_stb_order_agg['factor_1_stb'] = fully_HHs_hours_stb_order_agg['num_hhs'] /(
                                                                        fully_HHs_hours_stb_order_agg.fillna(0)['num_hhs_stb1']
                                                                        )
fully_HHs_hours_stb_order_agg['factor_2_stb'] = fully_HHs_hours_stb_order_agg['num_hhs'] /(                                                                     
                                                                         fully_HHs_hours_stb_order_agg.fillna(0)['num_hhs_stb2']
                                                                       )
fully_HHs_hours_stb_order_agg['factor_3_stb'] = fully_HHs_hours_stb_order_agg['num_hhs'] /(                                                                      
                                                                         fully_HHs_hours_stb_order_agg.fillna(0)['num_hhs_stb3']
                                                                       )
fully_HHs_hours_stb_order_agg['factor_4_stb'] = fully_HHs_hours_stb_order_agg['num_hhs'] /(                                                                      
                                                                         fully_HHs_hours_stb_order_agg.fillna(0)['num_hhs_stb4']
                                                                       )
fully_HHs_hours_stb_order_agg['factor_5_stb'] = fully_HHs_hours_stb_order_agg['num_hhs'] /(
                                                                         fully_HHs_hours_stb_order_agg.fillna(0)['num_hhs_stb5']
                                                                       )
fully_HHs_hours_stb_order_agg[fully_HHs_hours_stb_order_agg.num_hhs_stb1==0]
fully_HHs_hours_stb_order_agg[fully_HHs_hours_stb_order_agg['network_no']==9]

fully_HHs_hours_stb_order_agg2 = pd.merge(fully_HHs_hours_stb_order_agg[fully_HHs_hours_stb_order_agg.num_hhs_stb1!=0],
                                         num_hh_per_rep_stb_w.reset_index(),
                                         how= 'left',
                                         on = ['for_mso_no'])
fully_HHs_hours_stb_order_agg2['scaling'] =  fully_HHs_hours_stb_order_agg2['factor_1_stb']*fully_HHs_hours_stb_order_agg2['pct_hh_stb1'] +\
                   fully_HHs_hours_stb_order_agg2['factor_2_stb']*fully_HHs_hours_stb_order_agg2['pct_hh_stb2'] +\
                     fully_HHs_hours_stb_order_agg2['factor_3_stb']*fully_HHs_hours_stb_order_agg2['pct_hh_stb3'] +\
                     fully_HHs_hours_stb_order_agg2['factor_4_stb']*fully_HHs_hours_stb_order_agg2['pct_hh_stb4'] +\
                     fully_HHs_hours_stb_order_agg2['factor_5_stb']*fully_HHs_hours_stb_order_agg2['pct_hh_stb5']
by_net_scaling_factor = fully_HHs_hours_stb_order_agg2.groupby(['duration',
                                                                'for_mso_no',
                                                                'network_no'],
                                                               as_index=False).aggregate({'num_hhs':'sum',
                                                                                         'scaling':'mean'})
by_net_scaling_factor.rename(columns={'scaling':'scaling_factor',
                                     'for_mso_no':'mso_no',
                                     'num_hhs':'num_fully_hhs'},inplace=True)
networks = pd.read_csv('/home/jliu/projects/v_1_5/resproject_1121_scaling/reference_files/networks_20180701_rweek.txt',
                      sep = "|")
summarziable_networks = networks[(networks.summarize_from_date.isnull()==False)
#          &(pd.to_datetime(networks.summarize_from_date)<=pd.to_datetime('2017-08-28'))
        & ((networks.summarize_to_date.isnull()==True)|
              (pd.to_datetime(networks.summarize_to_date)>=pd.to_datetime('2017-08-28'))
          )]
summarziable_networks=summarziable_networks[summarziable_networks.network_no.isin([431, 5729, 5135,471,6365,6314,6423,434,
                                                                                  5065,5067,5063,5071,5069,322,21,22,23,24,26,413
                                                                                  ])==False]
################ by network scaling ######################

by_net_scaling_factor['from_date'] = np.where(by_net_scaling_factor.mso_no==12,
                                                  '2017-08-28',
                                                  '2018-01-29')
by_net_scaling_factor['to_date'] =''


out_dir = '/home/jliu/projects/v_1_5/resproject_1121_scaling/toup_files/output_files/'


#### by net 
by_net_scaling_factor_toup =by_net_scaling_factor.loc[(by_net_scaling_factor.num_fully_hhs>500)
                                                     &(by_net_scaling_factor.network_no.isin(summarziable_networks.network_no)),
                                                       ['mso_no','network_no', 'scaling_factor','from_date', 'to_date']]
by_net_scaling_factor_toup.to_csv(out_dir + 'live_by_net_mso_scaling_charter_twc_toup_20180712.txt',sep="|", index=False)

# default by net 
by_net_scaling_factor_toup =by_net_scaling_factor.loc[(by_net_scaling_factor.network_no==99999),
                                                       ['mso_no','scaling_factor','from_date', 'to_date']]
by_net_scaling_factor_toup.to_csv(out_dir + 'live_default_mso_scaling_charter_twc_toup_20180712.txt',
                              sep="|", index=False)


