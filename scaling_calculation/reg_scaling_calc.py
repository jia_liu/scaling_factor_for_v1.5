import pandas as pd
import numpy as np
def inital_clean(input_data):
    return  input_data[(input_data['num_reporting_stbs']<=input_data['total_num_stbs']) & 
                             (input_data['total_num_stbs']  !=0) &
                             (input_data['num_reporting_stbs']  !=0) &
                             (input_data['total_num_stbs']<=6)].copy()

def scaling_calc_reg (norm, denorm, merge_key):
    norm.rename(columns= {'hhs':'hhs_net'}, inplace=True)
    denorm.rename(columns= {'hhs':'hhs_all'}, inplace=True)
    net_allnet_duration_hhs = pd.merge(norm, 
                                       denorm, ## for dish and directv
                                       how='left', 
                                       on=merge_key, 
  #                                     validate="many_to_one"
					)
    net_allnet_duration_hhs['hhs_ratio'] = net_allnet_duration_hhs['hhs_net']/net_allnet_duration_hhs['hhs_all']
    # subset to fully reporting
    net_allnet_duration_hhs_full = net_allnet_duration_hhs[
                                net_allnet_duration_hhs['num_reporting_stbs'] == net_allnet_duration_hhs['total_num_stbs']].copy()
    net_allnet_duration_hhs_full.rename(columns= {'hhs_net':'hhs_net_full',
                                                  'hhs_all':'hhs_all_full',                                            
                                                  'hhs_ratio':'hhs_ratio_full'}, inplace=True)
    net_allnet_duration_hhs_calc1 = pd.merge(net_allnet_duration_hhs, 
                                          net_allnet_duration_hhs_full, 
                                          how='left', 
                                          on=['duration','mso_no', 'network_no','total_num_stbs'], 
   #                                       validate="many_to_one"
						)
    net_allnet_duration_hhs_calc1['hh_calc'] = np.maximum((net_allnet_duration_hhs_calc1['hhs_ratio_full']/
                                                      net_allnet_duration_hhs_calc1['hhs_ratio']).fillna(1), 
                                                         1)*net_allnet_duration_hhs_calc1['hhs_net']
    mso_by_net_scaling_factor=net_allnet_duration_hhs_calc1.groupby(['duration','mso_no', 'network_no'], 
                                                      as_index=False).agg({"hhs_net": "sum", 
                                                                           "hh_calc": "sum"})
    mso_by_net_scaling_factor['scaling_factor']= mso_by_net_scaling_factor['hh_calc']/mso_by_net_scaling_factor['hhs_net']
    mso_by_net_scaling_factor=pd.merge(mso_by_net_scaling_factor,
                                  net_allnet_duration_hhs_full.groupby(['duration','mso_no','network_no'],
                                                                       as_index=False).agg({"hhs_net_full": "sum"}),
                                   how='left',
                                  on = ['duration','mso_no','network_no'])
    return mso_by_net_scaling_factor.copy()
    

