
# coding: utf-8

# In[1]:


import pandas as pd
import os
import numpy as np
from gzip import GzipFile
import boto3
import io
import smart_open
import psutil
import gc
import sys


# In[2]:


hh_num_stbs = pd.DataFrame(pd.read_csv('/home/jliu/projects/v_1_5/scaling/hh_num_stbs_mso_4_102_114.txt', 
                                                    sep='|',
#                                                     nrows= 100,
                                                    dtype={1: str, 2: int, 3: int})
                                                    )
# hh_num_stbs.head()

hh_num_stbs_v2 = hh_num_stbs.groupby (['mso_no','hh_id'],
                                    as_index=False).agg({"total_num_stbs": "max"})
# raw data
hh_num_stbs_v2_mso_4 = hh_num_stbs_v2[hh_num_stbs_v2['mso_no']==4]
hh_num_stbs_v2_mso_102 = hh_num_stbs_v2[hh_num_stbs_v2['mso_no']==102]
del(hh_num_stbs)
del(hh_num_stbs_v2)
gc.collect()


# In[4]:


hh_num_stbs_v2_mso_102.head()


# In[5]:


hh_stb_repo = pd.DataFrame(pd.read_csv('/home/jliu/projects/v_1_5/scaling/hh_stb_3m_hours_mso_1_4_102.txt', 
                                                    sep='|',
#                                                     nrows= 100,
                                                    dtype={1: int, 2: int, 3: int, 4: str, 5: str})
                                                    )

hh_stb_repo['total_live_hours']=hh_stb_repo.fillna(0)['hours_m1']+hh_stb_repo.fillna(0)['hours_m2']+hh_stb_repo.fillna(0)['hours_m3']
hh_stb_repo['num_reporting_stbs'] = 1

# get num_reporting STBs
hh_repo_mso = hh_stb_repo.groupby(['mso_no','tv_market_no','headend_no','hh_id'],
                as_index=False).agg({"hours_m1": "sum", 
                "hours_m2": "sum",
                "hours_m3": "sum",
                "num_reporting_stbs":"count"})
# repo data 
hh_stb_repo_mso_1 = hh_stb_repo[hh_stb_repo['mso_no']==1]
hh_stb_repo_mso_4 = hh_stb_repo[hh_stb_repo['mso_no']==4]
hh_stb_repo_mso_102 = hh_stb_repo[hh_stb_repo['mso_no']==102]

hh_repo_mso_1 =hh_repo_mso[hh_repo_mso['mso_no']==1]
hh_repo_mso_4 =hh_repo_mso[hh_repo_mso['mso_no']==4]
hh_repo_mso_102 =hh_repo_mso[hh_repo_mso['mso_no']==102]
del(hh_stb_repo)
del(hh_repo_mso)
gc.collect()


# In[6]:


hh_total_rep_stbs_mso_4  = pd.merge(hh_repo_mso_4,
                            hh_num_stbs_v2_mso_4[['mso_no','hh_id','total_num_stbs']],
                            how = 'left',
                            on = ['mso_no','hh_id'],
                            validate= 'many_to_one')
hh_total_rep_stbs_mso_102  = pd.merge(hh_repo_mso_102,
                            hh_num_stbs_v2_mso_102[['mso_no','hh_id','total_num_stbs']],
                            how = 'left',
                            on = ['mso_no','hh_id'],
                            validate= 'many_to_one')

hh_total_rep_stbs_mso_102.head()
hh_total_rep_stbs_mso_4.head()


# In[33]:


hh_total_rep_stbs_mso_4[hh_total_rep_stbs_mso_4['num_reporting_stbs']>hh_total_rep_stbs_mso_4['total_num_stbs']].shape


# In[7]:


hh_pick_for_charter_mso_1= hh_repo_mso_1[(hh_repo_mso_1['hours_m1']> 0) & (hh_repo_mso_1['hours_m3']> 0)]


# In[8]:


hh_pick_for_charter_mso_4= hh_total_rep_stbs_mso_4[(hh_total_rep_stbs_mso_4['hours_m1']> 0) &
                                                   (hh_total_rep_stbs_mso_4['hours_m3']> 0) & 
                                                 (hh_total_rep_stbs_mso_4['total_num_stbs']== hh_total_rep_stbs_mso_4['num_reporting_stbs']) & 
                                                 (hh_total_rep_stbs_mso_4['total_num_stbs']> 0)
                                                    ]
hh_pick_for_charter_mso_4.shape


# In[9]:


hh_pick_for_charter_mso_102= hh_total_rep_stbs_mso_102[(hh_total_rep_stbs_mso_102['hours_m1']> 0) &
                                                       (hh_total_rep_stbs_mso_102['hours_m3']> 0) & 
                                                     (hh_total_rep_stbs_mso_102['total_num_stbs']== hh_total_rep_stbs_mso_102['num_reporting_stbs']) & 
                                                     (hh_total_rep_stbs_mso_102['total_num_stbs']> 0)
                                                    ]

hh_pick_for_charter_mso_102.shape


# In[10]:


national_hh_stb_pick_mso_1 = pd.merge (hh_stb_repo_mso_1[['mso_no','tv_market_no','headend_no','hh_id','stb_id','total_live_hours']],
                                hh_pick_for_charter_mso_1[['mso_no','tv_market_no','headend_no','hh_id','num_reporting_stbs']], 
                                how = 'inner',
                                on = ['mso_no','tv_market_no','headend_no','hh_id'],
                                validate= 'many_to_one'
                                )
rank_tmp = national_hh_stb_pick_mso_1.groupby(['mso_no','tv_market_no','headend_no','hh_id'], 
                                             as_index=False)['total_live_hours'].rank(ascending=False)
national_hh_stb_pick_mso_1['stb_order']=rank_tmp.reset_index(level=0, drop=True)
del(rank_tmp)
gc.collect()


# In[11]:


hh_stb_repo_mso_1.head()


# In[12]:


national_hh_stb_pick_mso_102 = pd.merge (hh_stb_repo_mso_102[['mso_no','tv_market_no','headend_no','hh_id','stb_id','total_live_hours']],
                                hh_pick_for_charter_mso_102[['mso_no','tv_market_no','headend_no','hh_id','num_reporting_stbs']], 
                                how = 'inner',
                                on = ['mso_no','tv_market_no','headend_no','hh_id'],
                                validate= 'many_to_one'
                                )
rank_tmp = national_hh_stb_pick_mso_102.groupby(['mso_no','tv_market_no','headend_no','hh_id'], 
                                             as_index=False)['total_live_hours'].rank(ascending=False)
national_hh_stb_pick_mso_102['stb_order']=rank_tmp.reset_index(level=0, drop=True)
del(rank_tmp)
gc.collect()


# In[13]:


national_hh_stb_pick_mso_4 = pd.merge (hh_stb_repo_mso_4[['mso_no','tv_market_no','headend_no','hh_id','stb_id','total_live_hours']],
                                hh_pick_for_charter_mso_4[['mso_no','tv_market_no','headend_no','hh_id','num_reporting_stbs']], 
                                how = 'inner',
                                on = ['mso_no','tv_market_no','headend_no','hh_id'],
                                validate= 'many_to_one'
                                )
rank_tmp = national_hh_stb_pick_mso_4.groupby(['mso_no','tv_market_no','headend_no','hh_id'], 
                                             as_index=False)['total_live_hours'].rank(ascending=False)
national_hh_stb_pick_mso_4['stb_order']=rank_tmp.reset_index(level=0, drop=True)
del(rank_tmp)
gc.collect()


# In[25]:


national_hh_stb_pick_mso_102.head()


# In[17]:


HH_STB_for_charter_tuner_ntnl = pd.concat([
                            national_hh_stb_pick_mso_1[['headend_no', 'hh_id', 'stb_id', 'num_reporting_stbs', 'stb_order']],
                            national_hh_stb_pick_mso_4[['headend_no', 'hh_id', 'stb_id', 'num_reporting_stbs', 'stb_order']],
                            national_hh_stb_pick_mso_102[['headend_no', 'hh_id', 'stb_id', 'num_reporting_stbs','stb_order']]
                                ])


# In[18]:


HH_STB_for_charter_tuner_ntnl.to_csv('/home/jliu/projects/v_1_5/scaling/HH_STB_for_charter_tuner_ntnl.txt', sep='|', index = False)


# In[ ]:


del(hh_stb_repo_mso_4_v1)
del(hh_stb_repo_mso_1_v1)
del(hh_stb_repo)
del(hh_num_stbs)

gc.collect()


# In[34]:


national_hh_stb_pick_mso_1.groupby (['mso_no','num_reporting_stbs']).count()


# In[ ]:





# In[25]:





# In[37]:


national_hh_stb_pick.head()


# In[35]:





# In[33]:


national_hh_stb_pick.shape


# In[36]:


national_hh_stb_tuner_main_tmp.shape

