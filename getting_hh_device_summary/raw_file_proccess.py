import boto3
import gzip
import io
import raw_file_proccess_func as ff

import pandas as pd
import numpy as np
import psutil
psutil.virtual_memory()
import gc
gc.collect()
####################################
##### DirecTV icd 51 ########
####################################
# aws s3 cp s3://csm-linear-raw-archive/2017_04/directv_audience_measurement/16/DIRECT_VIEW_ATTR_FULL_20170415.DAT.gz ./DIRECT_VIEW_ATTR_FULL_20170415.DAT.gz
# !aws s3 ls s3://csm-linear-raw-archive/2017_04/ --recursive  | grep 'DIRECT_VIEW_ATTR_FULL_20170415'
# icd_51_data = ff.read_icd51(s3_file_key_loc = '2017_04/directv_audience_measurement/16/DIRECT_VIEW_ATTR_FULL_20170415.DAT.gz')
# icd_51_data.to_csv('/home/jliu/projects/v_1_5/resproject_1121_scaling/intermediate_files/icd_51_data.txt', sep="|", index=False)
icd_51_data = pd.read_csv('/home/jliu/projects/v_1_5/resproject_1121_scaling/intermediate_files/icd_51_data.txt',sep="|", dtype = {0:str, 4:str})
dvr_icd51_composition = icd_51_data.groupby(['hh_id'], as_index=False).aggregate({'num_dvr_stbs':'sum',
                                                                                  'num_H_stbs':'sum', 
                                                                                  'num_HR_stbs':'sum'})
gc.collect()
psutil.virtual_memory()

########## dish dvr composition #######
dish_stb_composition_tmp = pd.read_csv('/home/pchen/2017/sep2017/dvr_h_r2/dish_dvr_account_201702_201704.psv', sep='|')
dish_stb_composition_tmp.rename(columns = {'total_num_dvr_capable_stb': 'num_dvr_stbs'}, inplace=True)
dish_stb_composition_tmp2=dish_stb_composition_tmp.groupby(['hh_id','num_dvr_stbs', 'total_num_stb'],as_index=False)['hh_no'].count()
dish_stb_composition_tmp3=dish_stb_composition_tmp2.groupby('hh_id',as_index=False).max()

dvr_dish_stb_composition = dish_stb_composition_tmp3[dish_stb_composition_tmp3.num_dvr_stbs>0]
hh_num_stbs = pd.concat([dvr_dish_stb_composition, dvr_icd51_composition])

################# repo for reporting STBs ################

########## repo data ####################
gc.collect()
psutil.virtual_memory()
cols1 = [  'mso_no',
        'tv_market_no',
        'headend_no',
        'hh_id',
        'repo_stb_id',
        'dvr_hours_bsd_m1',
        'dvr_hours_b24_m1',
        'dvr_hours_b48_m1',
        'dvr_hours_b72_m1',
        'dvr_hours_b168_m1',
        'dvr_hours_b360_m1',

        'dvr_hours_bsd_m2',
        'dvr_hours_b24_m2',
        'dvr_hours_b48_m2',
        'dvr_hours_b72_m2',
        'dvr_hours_b168_m2',
        'dvr_hours_b360_m2',

        'dvr_hours_bsd_m3',
        'dvr_hours_b24_m3',
        'dvr_hours_b48_m3',
        'dvr_hours_b72_m3',
        'dvr_hours_b168_m3',
        'dvr_hours_b360_m3',
        'dvr_hours_m1',
        'dvr_hours_m2',
        'dvr_hours_m3'
]


def read_repo_hh_stb(infile, dtype, names):
    data1 = pd.DataFrame(pd.read_csv(infile, 
                                     sep='|',
                                     dtype=dtype,
                                     header = None,
                                     names = names 
                          ))
    data1 ['stb_id']  = data1.repo_stb_id.str.split('.').str.get(0)
    data1 ['tuner_id']= data1.repo_stb_id.str.split('.').str.get(1).fillna(-1)
    data1 ['tuner_id']=data1 ['tuner_id'].astype(int)
    data1.rename(columns = {'dvr_hours_b360_m1':'hours_15d_m1', 'dvr_hours_b360_m2':'hours_15d_m2', 'dvr_hours_b360_m3':'hours_15d_m3'},inplace=True)
    print(psutil.virtual_memory())
    return data1[['mso_no',
        'tv_market_no',
        'headend_no',
        'hh_id',
        'stb_id',
        'tuner_id',
        'hours_15d_m1',
        'hours_15d_m2',
        'hours_15d_m3'
    ]].copy()


dvr_rep_stbs_national_tmp0 = read_repo_hh_stb(infile = '/data_storage/home/pchen/2017/sep2017/dvr_h_r2/repo/dvr_rep_stbs_sept2017r2_all.txt',
                   dtype={0: int, 1: int, 2: int, 3: str, 4: str},
                   names = cols1)
print(psutil.virtual_memory())
gc.collect()

dvr_rep_stbs_national_tmp0['hours_15d_3months']=dvr_rep_stbs_national_tmp0.hours_15d_m1.fillna(0)+\
                                                dvr_rep_stbs_national_tmp0.hours_15d_m2.fillna(0)+\
                                                dvr_rep_stbs_national_tmp0.hours_15d_m3.fillna(0)
dvr_rep_stbs_national_tmp = dvr_rep_stbs_national_tmp0[dvr_rep_stbs_national_tmp0.hours_15d_3months>0].groupby(['mso_no', 'tv_market_no', 'headend_no', 'hh_id','stb_id'], as_index=False).aggregate({'hours_15d_m1':'sum', 'hours_15d_m2':'sum', 'hours_15d_m3':'sum', 'hours_15d_3months':'sum'})


dvr_rep_stbs_national_tmp=pd.merge(dvr_rep_stbs_national_tmp,
                                   icd_51_data,
                                   how = 'left',
                                   on = ['hh_id','stb_id'])
dvr_rep_stbs_national_tmp['num_H_hours']= np.where(dvr_rep_stbs_national_tmp.num_H_stbs==1, dvr_rep_stbs_national_tmp.hours_15d_3months, 0)

dvr_rep_stbs_national= dvr_rep_stbs_national_tmp.groupby(['mso_no', 'tv_market_no', 'headend_no', 'hh_id'], 
                                                   as_index=False).aggregate({'hours_15d_m1':'sum', 'hours_15d_m2':'sum', 'hours_15d_m3':'sum', 
                                                 'stb_id':'count', 'num_H_hours':'sum'})       
                            
dvr_rep_stbs_national.rename(columns = {'stb_id':'num_reporting_stbs', 'hours_15d_m1':'hours_m1','hours_15d_m2':'hours_m2','hours_15d_m3':'hours_m3'}, inplace=True)
print(psutil.virtual_memory())

dvr_national_hh_compo = pd.merge(dvr_rep_stbs_national, hh_num_stbs, 
                                  on = 'hh_id', 
                                   how = 'left' )



dvr_national_hh_compo['num_stbs'] = np.where((dvr_national_hh_compo.mso_no == 102) & (dvr_national_hh_compo.num_H_hours>0), 
                                              dvr_national_hh_compo.num_dvr_stbs.fillna(0) + dvr_national_hh_compo.num_H_stbs.fillna(0), 
                                              dvr_national_hh_compo.num_dvr_stbs.fillna(0) )

dvr_national_hh_compo_pick= dvr_national_hh_compo[(dvr_national_hh_compo.hours_m1>0) & (dvr_national_hh_compo.hours_m3>0)]
dvr_national_hh_compo_pick = dvr_national_hh_compo_pick.loc[(dvr_national_hh_compo.hours_m1>0) & (dvr_national_hh_compo.hours_m3>0), ['headend_no', 'hh_id', 'num_reporting_stbs', 'num_stbs']]
dvr_national_hh_compo_pick.to_csv('/home/jliu/projects/v_1_5/resproject_1121_scaling/repo_data/DVR_scaling/r2/dvr_hh_compo_sept2017_v2.txt', sep = "|", index=False)
############### end for DVR ############
dvr_national_hh_compo_pick2= pd.read_csv('/home/jliu/projects/v_1_5/resproject_1121_scaling/repo_data/DVR_scaling/r2/dvr_hh_compo_sept2017.txt',sep="|",dtype={0:'int', 1:'str'})

dvr_national_hh_compo_pick2.rename(columns={'num_reporting_stbs':'num_reporting_stbs2','num_stbs':'num_stbs2'},inplace=True)
dvr_national_hh_compo_pick2['num_stbs2']=dvr_national_hh_compo_pick2.num_stbs2.fillna(0)

dvr_national_hh_compo_pick2[dvr_national_hh_compo_pick2.num_stbs2.isna()]
dvr_national_hh_compo_pick2[dvr_national_hh_compo_pick2.hh_id=='4599685255']
comp= pd.merge(dvr_national_hh_compo_pick, dvr_national_hh_compo_pick2, on= ['hh_id', 'headend_no'], how= 'outer' )
diff = comp[(comp.num_reporting_stbs!=comp.num_reporting_stbs2) | (comp.num_stbs!=comp.num_stbs2)]


comp[comp.hh_id=='4599685255']


test_hh='3104370190'
dvr_rep_stbs_national_tmp[dvr_rep_stbs_national_tmp.hh_id==test_hh]

icd_51_data[icd_51_data.hh_id==test_hh]
dvr_national_hh_compo[dvr_national_hh_compo.hh_id==test_hh]


dvr_icd51_composition[dvr_icd51_composition.hh_id==test_hh]

dvr_national_hh_compo[dvr_national_hh_compo.hh_id==test_hh]
dvr_national_hh_compo_pick[dvr_national_hh_compo_pick.hh_id==test_hh]


comp[comp.hh_id=='1006615159']


a=dvr_national_hh_compo_pick[dvr_national_hh_compo_pick.hh_id== '+/90mcBupqIM8z4LrBhiudYhFlc=']
b=dvr_national_hh_compo_pick2[dvr_national_hh_compo_pick2.hh_id== '+/90mcBupqIM8z4LrBhiudYhFlc=']

comp= pd.merge(a, b, on= ['hh_id', 'headend_no'], how= 'outer' )
################
####################################
##### DirecTV genie clinets ########
####################################
# !aws s3 ls s3://csm-linear-raw-archive/2017_04/directv_audience_measurement/16/
genie_stb_f=smart_open.smart_open('s3://csm-linear-raw-archive/2017_04/directv_audience_measurement/16/BA_RENTRAK_ACCESS_CARD_WITH_CLIENTS_20170416.DAT.gz')
genie_stb = pd.read_csv(genie_stb_f,sep = "|", dtype={0: str},)
genie_stb['raw_num_genie_and_clients']=genie_stb['WIRELESS_CLIENT_CNT'].fillna(0) \
                            + genie_stb['WIRED_CLIENT_CNT'].fillna(0)+1
genie_stb.rename(columns={
                          'ACRD_ID':'stb_id'
                         }, inplace=True)
del(genie_stb['WIRELESS_CLIENT_CNT'])
del(genie_stb['WIRED_CLIENT_CNT'])
del(genie_stb['ADV_WH_DVR_RVUTV_CNT'])

directv_num_stb_per_hh_tmp1 = pd.merge(icd_51_data,
                                 genie_stb,
                                 on = 'stb_id',
                                 how='outer'
                                )
# !aws s3 ls s3://csm-linear-raw-archive/2017_04/linear_cox/ --recursive  | grep 'ff_vid_customer_device_export_20170430' 
####################################
##### Cox Account Files ########
####################################
cox_raw_device = ff.read_cox_raw_acc(s3_file_key_loc = '2017_04/linear_cox/30/ff_vid_customer_device_export_20170430.dat.gz')
cox_raw_device.columns
cox_num_stbs_per_hh = cox_raw_device.groupby(['hh_id'],as_index=False).agg({'stb_id':'count'})
cox_num_stbs_per_hh.columns = ['hh_id','total_num_stbs']
cox_num_stbs_per_hh.head()
# del(cox_raw_device)
# gc.collect()
####################################
######### Dish account file ########
####################################
dish_stb_composition_tmp = pd.DataFrame(pd.read_csv('/data_storage/work/cleffers/sept2017/H_Factors/Round2/reporting_vs_non_reporting_by_hh_clean.zip', 
                                                    nrows=5000,
                                                    sep='|'))

cox_num_stbs_per_hh['mso_no']=114
directv_num_stb_per_hh['mso_no']=102
dish_stb_composition_tmp['mso_no']=4

hh_num_stbs = pd.concat ([cox_num_stbs_per_hh,
                        directv_num_stb_per_hh, 
                         dish_stb_composition_tmp[['hh_id','total_num_stbs','mso_no']]
                         ])
# hh_num_stbs.head()
hh_num_stbs.to_csv('/home/jliu/projects/v_1_5/scaling/hh_num_stbs_mso_4_102_114.txt', sep='|')









# print(Counter(chain.from_iterable(dic.keys() for dic in hh_id_key_stb_id_list)))

# icd_51_data.head()

# directv_num_stb_per_hh_icd51 = icd_51_data.groupby(['hh_id'],as_index=False).agg({'stb_id':'count'})
# directv_num_stb_per_hh_icd51.columns = ['hh_id','total_num_stbs']
# directv_num_stb_per_hh_icd51.head()
# del(icd_51_data)
# gc.collect()

# 'H44-100','H44-500','HR34-700','HR44-200','HR44-500','HR44-700','HR54-200','HR54-500','HR54-700'
