
import pandas as pd
import os
import numpy as np
from gzip import GzipFile
import boto3
import io
import smart_open

import gc
import sys

import gzip
from itertools import islice
from itertools import chain
from collections import Counter
import datetime
from zipfile import ZipFile
import dask.dataframe as dd

import inspect


import psutil
psutil.virtual_memory()
import zipfile

national_1_1= read_hh_stb_month_view(filename =ZipFile('/home/jwong/work/PROJECTS/2017_TWC_H/Repo_pull/repo_hh_compo_month_june_resfactors356_linear_national.zip'), 
                                     sub_file='repo_hh_compo_month_june_resfactors356_linear_national.txt')
national_1_1.rename(columns={'num_hours':'hours_m1'}, inplace=True)


hh_id_key_stb_id_list=[] #{} #'hh_id':'num_stb_id'
hh_id_key_stb_id_dict={}

with zipfile.ZipFile('/home/jwong/work/PROJECTS/2017_TWC_H/Repo_pull/repo_hh_compo_month_june_resfactors356_linear_national.zip') as z:
    with z.open('repo_hh_compo_month_june_resfactors356_linear_national.txt') as f:
        for line in f:
            print(line, [psutil.virtual_memory()])
chunksize = 10 ** 6
for chunk in pd.read_csv(filename, chunksize=chunksize):
    process(chunk)



def read_hh_stb_month_view(filename, sub_file):

    with filename.open(sub_file) as f:
        for idx, line_byte in enumerate(f):
            hh_id_key_stb_id_dict={}
    #         if idx ==0: continue;
            line_obj=io.BytesIO(line_byte)
    #         print("idx=", idx, type(line_obj.getvalue().decode("utf-8")),"\n",  line_obj.getvalue().decode("utf-8") )
    #         print("\nidx=", idx)
            line_str=line_obj.getvalue().decode("utf-8")
    #         print(line_str)
    #         line_strlist_aftersplit=line_str.split('|')
            mso_no=int(line_str.split('|')[0])
            repo_STB_id=line_str.split('|')[4]
            HH_id=line_str.split('|')[3]   
            num_hours=float(line_str.split('|')[6])
    #         print("HH_id=",HH_id,"STB_id=",STB_id,"num_hours=",num_hours,"mso_no=",mso_no) 
    #         print("\nidx=", STB_id.split('.')[0], 'a', STB_id.split('.')[1])
            if mso_no == 4: 
                STB_id= repo_STB_id.split('.')[0]
                tuner_id=int(repo_STB_id.split('.')[1])
            else: 
                STB_id= repo_STB_id
                tuner_id=0
            hh_id_key_stb_id_dict['mso_no']=mso_no
            hh_id_key_stb_id_dict['hh_id']=HH_id
            hh_id_key_stb_id_dict['stb_id']=STB_id
            hh_id_key_stb_id_dict['tuner_id']=tuner_id
            hh_id_key_stb_id_dict['num_hours']=num_hours
            hh_id_key_stb_id_list.append(hh_id_key_stb_id_dict)
    #         print("hh_id_key_stb_id_dict", hh_id_key_stb_id_dict)
            #print("hh_id_key_stb_id_list", hh_id_key_stb_id_list)
    #         if idx>5 : break;
            if idx%1000000==0 : print("idx=",idx,"memory_used_pct",psutil.virtual_memory()[2] ,str(datetime.datetime.now()));
    return pd.DataFrame(hh_id_key_stb_id_list)  


# In[ ]:


national_1_1= read_hh_stb_month_view(filename =ZipFile('/home/jwong/work/PROJECTS/2017_TWC_H/Repo_pull/repo_hh_compo_month_june_resfactors356_linear_national.zip'), 
                                     sub_file='repo_hh_compo_month_june_resfactors356_linear_national.txt')
national_1_1.rename(columns={'num_hours':'hours_m1'}, inplace=True)


# In[17]:


national_1_1= read_hh_stb_month_view(filename =ZipFile('/home/jwong/work/PROJECTS/2017_TWC_H/Repo_pull/repo_hh_compo_month_june_resfactors356_linear_national.zip'), 
                                     sub_file='repo_hh_compo_month_june_resfactors356_linear_national.txt')
national_1_1.rename(columns={'num_hours':'hours_m1'}, inplace=True)



national_1_3 =dd.read_csv('/home/jwong/work/PROJECTS/2017_TWC_H/Repo_pull/repo_hh_compo_month_august_resfactors356_linear_national.zip', 
                                                    sep='|',
                                                    dtype={0: int, 1: int, 2: int, 3: str, 4: str},
#                                                     nrows= 100,
                                                    header = None)




national_1_2= read_hh_stb_month_view(filename =ZipFile('/home/jwong/work/PROJECTS/2017_TWC_H/Repo_pull/repo_hh_compo_month_july_resfactors356_linear_national.zip'), 
                                     sub_file='repo_hh_compo_month_july_resfactors356_linear_national.txt')
national_1_2.rename(columns={'num_hours':'hours_m2'}, inplace=True)
import dask.dataframe as dd

national_1_1_2=pd.merge(national_1_1, 
                national_1_2, 
                how='outer', 
                on=['mso_no','hh_id', 'stb_id'], 
                validate="one_to_one")
national_1_1_2.head()
# del(national_1_2_clean)
# del(national_1_1_clean)
# gc.collect()

national_1_3= read_hh_stb_month_view(filename =ZipFile('/home/jwong/work/PROJECTS/2017_TWC_H/Repo_pull/repo_hh_compo_month_august_resfactors356_linear_national.zip'), 
                                     sub_file='repo_hh_compo_month_august_resfactors356_linear_national.txt')
national_1_3_clean.rename(columns={'num_hours':'hours_m3'}, inplace=True)

national_hh_stb_tuner_main_tmp = pd.merge(
            national_1_1_2,
            national_1_3, 
            how='outer', 
            on=['mso_no','hh_id', 'stb_id'], 
            validate="one_to_one")
del(national_1_1_2)
del(national_1_3)
gc.collect()
national_hh_stb_tuner_main_tmp.to_csv('/home/jliu/projects/v_1_5/scaling/hh_stb_3m_hours_mso_1_4_102.txt', sep='|')



national_1_1_clean=national_1_1.groupby(['mso_no','hh_id', 'stb_id'], 
                                        as_index=False).agg({"num_hours": "sum"})
# national_1_1_clean=national_1_1_clean[national_1_1_clean['mso_no'].isin([1, 4, 102]) ]
del(national_1_1)
gc.collect()


national_1_2_clean=national_1_2.groupby(['mso_no','hh_id', 'stb_id'], 
                                        as_index=False).agg({"num_hours": "sum"})
# national_1_2_clean=national_1_2_clean[national_1_2_clean['mso_no'].isin([1, 4, 102]) ]

del(national_1_2)
gc.collect()




national_1_3 = pd.DataFrame(pd.read_csv('/home/jwong/work/PROJECTS/2017_TWC_H/Repo_pull/repo_hh_compo_month_august_resfactors356_linear_national.zip', 
                                                    sep='|',
                                                    dtype={0: int, 1: int, 2: int, 3: str, 4: str},
#                                                     nrows= 100,
                                                    header = None))
national_1_3.columns = ['mso_no','tv_market_no','headend_no','hh_id', 'repo_stb_id', 'num_stbs', 'hours_m3']


national_1_3=national_1_3[national_1_3['mso_no'].isin([1, 4, 102]) ]
national_1_3['stb_id']  =national_1_3.repo_stb_id.str.split('.').str.get(0)
# national_1_3['tuner_id']=national_1_3.repo_stb_id.str.split('.').str.get(1)
national_1_3_clean=national_1_3.groupby(['mso_no','tv_market_no','headend_no','hh_id', 'stb_id'], 
                                        as_index=False).agg({"hours_m3": "sum"})

# national_1_3[(national_1_3['mso_no'] != 4) & 
#             (national_1_3['tuner_id'].notnull())]

# national_1_3.groupby(['mso_no']).count()
# national_1_3_clean.groupby(['mso_no']).count()

del(national_1_3)
gc.collect()

# del(national_1_1)
# del(national_1_2)
# del(national_1_3)

# del(national_1_1_clean)
# del(national_1_2_clean)
# del(national_1_3_clean)

# del(national_1_1_2)
# gc.collect()



## the above manipulation is to get the equivalent data if could pull from 3month repo

national_hh_stb_tuner_main_tmp = pd.DataFrame(pd.read_csv('/home/jliu/projects/v_1_5/scaling/hh_stb_3m_hours_mso_1_4_102.txt', 
                                                    sep='|',
                                                    dtype={0: int, 1: int, 2: int, 3: str, 4: str},
#                                                     nrows= 100)
                                                    )


# In[9]:


# national_hh_stb_tuner_main_tmp_2 = pd.DataFrame(pd.read_csv('/home/jwong/work/PROJECTS/2017_TWC_H/Repo_pull/repo_hh_compo_resfactors356_charter_charter.zip', 
# #                                                     nrows= 100,
#                                                     sep='|',
#                                                     dtype={0: int, 1: int, 2: int, 3: str, 4: str},
#                                                     header = None))
# national_hh_stb_tuner_main_tmp_3 = pd.DataFrame(pd.read_csv('/home/jwong/work/PROJECTS/2017_TWC_H/Repo_pull/TWC_repo_03/repo_hh_compo_resfactors356_twc_standby.zip', 
# #                                                     nrows= 100,
#                                                     dtype={0: int, 1: int, 2: int, 3: str, 4: str},
#                                                     sep='|',
#                                                     header = None))
# national_hh_stb_tuner_main_tmp_2.columns = ['mso_no','tv_market_no','headend_no','hh_id', 'repo_stb_id', 'num_stbs','hours_m1','hours_m2','hours_m3']
# national_hh_stb_tuner_main_tmp_3.columns = ['mso_no','tv_market_no','headend_no','hh_id', 'repo_stb_id', 'num_stbs','hours_m1','hours_m2','hours_m3']
# national_hh_stb_tuner_main_tmp_1=pd.concat([national_hh_stb_tuner_main_tmp,
#                                            national_hh_stb_tuner_main_tmp_2,
#                                            national_hh_stb_tuner_main_tmp_3])



national_hh_stb_tuner_main_tmp['total_live_hours']=national_hh_stb_tuner_main_tmp.fillna(0)['hours_m1']+
                                                    national_hh_stb_tuner_main_tmp.fillna(0)['hours_m2']+
                                                    national_hh_stb_tuner_main_tmp.fillna(0)['hours_m3']


# In[ ]:


national_hh_stb_tuner_main_tmp_samp = national_hh_stb_tuner_main_tmp[national_hh_stb_tuner_main_tmp['tv_market_no']==500]
a=national_hh_stb_tuner_main_tmp_samp.groupby(['mso_no','tv_market_no','headend_no','hh_id'], 
                                              as_index=False)['total_live_hours'].rank(ascending=False)
national_hh_stb_tuner_main_tmp_samp['stb_order']=a.reset_index(level=0, drop=True)


# In[59]:


national_hh_stb_tuner_main_tmp['stb_order']=national_hh_stb_tuner_main_tmp.groupby(['mso_no','tv_market_no','headend_no','hh_id'], as_index=False)['total_live_hours'].rank(ascending=False)


# In[45]:


national_hh_mso_1_102_4 = national_hh_stb_tuner_main_tmp.groupby(['mso_no','tv_market_no','headend_no','hh_id'],
                                                                 as_index=False).agg({"hours_m1": "sum", 
                                                                                     "hours_m2": "sum",
                                                                                     "hours_m3": "sum"})


# In[50]:


# subset to mso_no in (1,4,102)
# subset to fully reporting HHs
# subset to HHs with viewing in first and third month





hh_compo
	where (mso_no in (1,4,102) and num_stbs=num_reporting_stbs and num_stbs ^= 0);
national_hh_compo_pick


# In[58]:


national_hh_stb_tuner_main_tmp_samp.head()


# In[53]:


national_hh_stb_tuner_main_tmp_samp.head()


# In[14]:


national_hh_stb_tuner_main_tmp_samp['+/90mcBupqIM8z4LrBhiudYhFlc=']

