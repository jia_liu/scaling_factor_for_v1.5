
# coding: utf-8

# In[ ]:


get_ipython().run_line_magic('reset', '')


# In[1]:


import pandas as pd
import os
import numpy as np
from gzip import GzipFile
import boto3
import io
import smart_open
import psutil
import gc

######### by net files ######
###### step 1 calculate 1stb - 6stb scaling from Dish, DirecTV and ATT fully reporting STBs


# In[ ]:



import sys
import pandas as pd
#import boto3
import io
import gzip
from itertools import islice
from itertools import chain
from collections import Counter

import gzip
import io
from collections import Counter

filename= '/mnt/nfs/corp_sas_live/mounts/data_storage/home/jliu/projects/v_1_5/scaling/Soyoung_DIRECT_VIEW_ATTR_FULL_20170815.DAT.gz'

hh_id_key_stb_id_list=[] #{} #'hh_id':'num_stb_id'
hh_id_key_stb_id_dict={}

with gzip.open(filename, 'rb') as f:
    for idx, line_byte in enumerate(f):
        hh_id_key_stb_id_dict={}
        if idx < 427: continue;
        line_obj=io.BytesIO(line_byte)
        #print("idx=", idx, type(line_obj.getvalue().decode("utf-8")),"\n",  line_obj.getvalue().decode("utf-8") )
        #print("\nidx=", idx)
        line_str=line_obj.getvalue().decode("utf-8")
        line_strlist_aftersplit=line_str.split()
        STB_id=line_str.split()[0]
        HH_id=line_str.split()[2]        
        #print("HH_id=",HH_id,"STB_id=",STB_id)   
        hh_id_key_stb_id_dict[HH_id]=STB_id
        hh_id_key_stb_id_list.append(hh_id_key_stb_id_dict)
        #print("hh_id_key_stb_id_dict", hh_id_key_stb_id_dict)
        #print("hh_id_key_stb_id_list", hh_id_key_stb_id_list)
        
        if idx%1000==0 : print("idx=",idx);

print(Counter(chain.from_iterable(dic.keys() for dic in hh_id_key_stb_id_list)))


# In[12]:


####################################
######### Cox account file #########
####################################
# cd /home/jliu/projects/v_1_5/scaling/
# aws s3 ls s3://csm-linear-raw-archive/2017_08/linear_cox/27/ --recursive  | grep 'ff_vid_customer_device_export_20170827'   
# aws s3 cp s3://csm-linear-raw-archive/2017_08/linear_cox/27/ff_vid_customer_device_export_20170827.dat.gz ./ff_vid_customer_device_export_20170827.dat.gz
cox_raw_f=smart_open.smart_open('s3://csm-linear-raw-archive/2017_04/linear_cox/30/ff_vid_customer_device_export_20170430.dat.gz')
cox_raw_device = pd.read_csv(cox_raw_f,sep = "|")
cox_raw_device.columns =['site', 'zip_cd', 'hh_id', 'stb_id', 'item_nbr', 
                         'item_descr', 'trio', 'rovi', 'hd_flg', 
                         'dvr_flg', 'moto', 'cisco', 'vod_addr', 'CONTOUR2_FLG']

# cox_raw_device.head()
# # cox_raw_device.columns
# cox_num_stbs_per_hh = cox_raw_device.groupby(['hh_id'],as_index=False).agg({'stb_id':'count'})
# cox_num_stbs_per_hh.columns = ['hh_id','total_num_stbs']
# cox_num_stbs_per_hh.head()
# del(cox_raw_device)
# gc.collect()


cox_raw_device[cox_raw_device.item_nbr.isin(['CPX022','CMX013','MMX013',
                                             'GSX9865','GMX9865','MPX022','CMH100',
                                             'CMX011','MMX011T','CMH100'
])].groupby(['item_nbr', 
                         'item_descr', 'trio', 'rovi', 'hd_flg', 
                         'dvr_flg', 'moto', 'cisco',  'CONTOUR2_FLG'],as_index=False)['site'].count()



cox_raw_device[cox_raw_device.CONTOUR2_FLG=='Y'].groupby(['item_nbr', 
                         'item_descr', 'trio', 'rovi', 'hd_flg', 
                         'dvr_flg', 'moto', 'cisco',  'CONTOUR2_FLG'],as_index=False)['site'].count()


cox_raw_device.groupby(['item_nbr','dvr_flg'],as_index=False)['site'].count().to_csv('cox_model_dvr_raw.txt',
                                                                          sep="|",index=False)


####################################
######### DirecTV icd - 51 #########
####################################
#  aws s3 ls s3://csm-linear-raw-archive/2017_08/ --recursive  | grep 'DIRECT_VIEW_ATTR_FULL_20170815'    


aws s3 cp s3://csm-linear-raw-archive/2017_08/directv_audience_measurement/16/DIRECT_VIEW_ATTR_FULL_20170815.DAT.gz DIRECT_VIEW_ATTR_FULL_20170815.DAT.gz

icd_f = '/home/jliu/projects/v_1_5/scaling/DIRECT_VIEW_ATTR_FULL_20170815.DAT.gz'
# read first 5000 rows to get header info
to_determine_data_start = pd.read_fwf(icd_f, 
                      widths=[12], 
                      header=None, 
                      nrows = 5000,
                     compression = 'gzip')

data_start = to_determine_data_start[to_determine_data_start[0]=='DEMOGRAPHICS'].index.tolist()
# Extract header informations
header_info1 = pd.read_fwf(icd_f, 
                      widths=[10,140,5,5,6,100, 3], 
                      header=None, 
                        skiprows = [0,1],
                      nrows = data_start[0]-1,
                     compression = 'gzip')
header_info1.columns = ['name','descpt','column','len','F5','F6', 'values']
# dedup header
header_info2=header_info1.groupby(['column','name','descpt','len'], as_index=False).agg({"F5": "count"})
# first 3 columns info was not in the first part of icd 51, manually add them
header_info_extra = pd.DataFrame( {'column': [1, 20,50], 
                                        'name'  : ['stb_id', 'op','hh_id'],
                                        'descpt': ['stb_id', 'op','hh_id'],
                                        'len'   : [19, 30, 35],
                                        'F5'    : [1,1, 1]})
## complete header info
header_info3 = pd.concat([header_info_extra,header_info2])
width_ind = header_info3.sort_values(by=['column'])['len'].astype(int)
# read in icd 51 
# for H/Scaling factor, only care about the first 3 columns
icd_51_data = pd.read_fwf(icd_f, 
                      widths=width_ind.values[0:3], 
                      header=None, 
#                         nrows = 5000,
                      skiprows = range(data_start[0]+1),
                        dtype={0: str, 1: str, 2: str},
                     compression = 'gzip')

icd_51_data.columns = header_info3.sort_values(by=['column'])['name'][0:3]
# header_info3.sort_values(by=['column'])['name'][0:5]
icd_51_data.head()
# icd_51_data.shape
# icd_51_data.describe()
directv_num_stb_per_hh = icd_51_data.groupby(['hh_id'],as_index=False).agg({'stb_id':'count'})
directv_num_stb_per_hh.columns = ['hh_id','total_num_stbs']
directv_num_stb_per_hh.head()
del(icd_51_data)
gc.collect()


# In[39]:


####################################
######### Dish account file ########
####################################
dish_stb_composition_tmp = pd.DataFrame(pd.read_csv('/home/jwong/work/PROJECTS/2017_TWC_H/reporting_vs_non_reporting_by_hh_clean.zip', 
#                                                     nrows=5000,
                                                    sep='|'))

cox_num_stbs_per_hh['mso_no']=114
directv_num_stb_per_hh['mso_no']=102
dish_stb_composition_tmp['mso_no']=4


hh_num_stbs = pd.concat ([cox_num_stbs_per_hh,
                        directv_num_stb_per_hh, 
                         dish_stb_composition_tmp[['hh_id','total_num_stbs','mso_no']]
                         ])
# hh_num_stbs.head()
hh_num_stbs.to_csv('/home/jliu/projects/v_1_5/scaling/hh_num_stbs_mso_4_102_114.txt', sep='|')


del(cox_num_stbs_per_hh)
del(directv_num_stb_per_hh)
del(dish_stb_composition_tmp)
gc.collect()



