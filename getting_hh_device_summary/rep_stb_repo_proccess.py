import pandas as pd
import numpy as np

# Repo viewing data 
def read_repo_hh_stb(infile, dtype, names):
    data1 = pd.DataFrame(pd.read_csv(infile, 
                                     sep='|',
                                     dtype=dtype,
                                     header = None,
                                     names = names 
                          ))
    data1 ['stb_id']  = data1.repo_stb_id.str.split('.').str.get(0)
    data1 ['tuner_id']= np.where(data1 .repo_stb_id.str.split('.').str.get(1).notna(),
                                 data1 .repo_stb_id.str.split('.').str.get(1),
                                 '-1')
    data1 ['tuner_id']=data1 ['tuner_id'].astype(int)
    return data1 



















dtv_live_op = read_repo_hh_stb('/home/jliu/projects/v_1_5/scaling/sep2017/repo_hh_compo_resfactors148_op_linear_directv.txt.gz',
                   dtype={0: int, 1: int, 2: int, 3: str, 4: str},
                   names = ['mso_no','tv_market_no','headend_no','hh_id',
                            'repo_stb_id', 'num_stbs', 'hours_m1', 'hours_m2', 'hours_m3']
)
dtv_live_op1=dtv_live_op.groupby(['hh_id', 'stb_id','tuner_id'],as_index=False)['hours_m1',
                                                                                'hours_m2',
                                                                                'hours_m3'].sum()


dtv_live_op1['view']='live'

dtv_live_op2=dtv_live_op1.groupby(['hh_id', 'stb_id'],as_index=False)['view'].count()
dtv_live_op2.rename(columns = {'view':'repo_num_genie_and_clients'},inplace=True)
repo_hh_list_tmp =  dtv_live_op2.groupby(['hh_id'],as_index=False)['repo_num_genie_and_clients'].sum()
repo_hh_list=repo_hh_list_tmp[(repo_hh_list_tmp.hours_m1>0) 
                              &(repo_hh_list_tmp.hours_m3>0)  ]
