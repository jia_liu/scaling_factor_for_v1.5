###  python34 -m pip install boto3 --user
import boto3
import gzip
import pandas as pd
import numpy as np
import io
import psutil
psutil.virtual_memory()
############################################################################################# DirecTV ICD-51 ###########################################################################################################
def read_icd51(s3_file_key_loc):
 s3_file_key= s3_file_key_loc
 bucket = 'csm-linear-raw-archive'
 s3 = boto3.client('s3')
 #################################################################################################
 ###### getting the start line of data for icd-51 #########
 obj = s3.get_object(Bucket=bucket, Key=s3_file_key)
 obj_r = io.BytesIO(obj['Body'].read(1000000))
 with gzip.open(obj_r, 'rb') as f:
     for idx, line_byte in enumerate(f):
         if b'DEMOGRAPHICS' in line_byte: 
             start_line = idx + 1
             break
 print(start_line)       
 #################################################################################################
 ######## model list useful for DirecTV DVR scaling factor ######
 H_models = ['H25-500','H25-100','H25-700','H24-200',
 	'H24-700','H24-100','H23-600',
 	'H21-200','H21-100','H44-500']
 HR_models = ['HR20', 'HR21', 'HR22', 'HR23', 'HR24','HR34']
 #################################################################################################
 ######## starting to read icd-51 ##########
 obj = s3.get_object(Bucket=bucket, Key=s3_file_key)
 obj_r = io.BytesIO(obj['Body'].read())
 hh_id_key_stb_id_list=[] #{} #'hh_id':'num_stb_id'
 hh_id_key_stb_id_dict={}
 with gzip.open(obj_r, 'rb') as f:
     for idx, line_byte in enumerate(f):
         hh_id_key_stb_id_dict={}
         if idx < start_line: continue
         line_obj=io.BytesIO(line_byte)
         line_str=line_obj.getvalue().decode("utf-8")
         line_strlist_aftersplit=line_str.split()
         STB_id=line_str.split()[0]
         HH_id=line_str.split()[2]    
         stb_model=line_str.split()[3][1:]
         if 'R' in stb_model:
          num_dvr_stbs = 1
         else:
          num_dvr_stbs=0
         if stb_model in H_models:
          num_H_stbs = 1
         else:
          num_H_stbs=0      
         if any (HR_model in stb_model for HR_model in HR_models):
          num_HR_stbs = 1
         else:
          num_HR_stbs=0
         hh_id_key_stb_id_dict['hh_id']=HH_id
         hh_id_key_stb_id_dict['stb_id']=str(int(STB_id))
         hh_id_key_stb_id_dict['stb_model']=stb_model
         hh_id_key_stb_id_dict['num_dvr_stbs']=num_dvr_stbs
         hh_id_key_stb_id_dict['num_H_stbs']=num_H_stbs
         hh_id_key_stb_id_dict['num_HR_stbs']=num_HR_stbs
        # if 'HR' in stb_model:
        #  print(stb_model, num_HR_stbs)
         hh_id_key_stb_id_list.append(hh_id_key_stb_id_dict)
         if idx%1000000==0 : print("idx=",idx,'memory_used',psutil.virtual_memory()[2],  
                                   hh_id_key_stb_id_dict)
 icd_51_data = pd.DataFrame(hh_id_key_stb_id_list)
 return icd_51_data


####################################
######### Cox account file #########
####################################
def read_cox_raw_acc(s3_file_key_loc):
 s3_file_key =s3_file_key_loc
 bucket = 'csm-linear-raw-archive'
 s3 = boto3.client('s3')   
 obj = s3.get_object(Bucket=bucket, Key=s3_file_key)
 obj_r = io.BytesIO(obj['Body'].read())
 hh_id_key_stb_id_list=[] #{} #'hh_id':'num_stb_id'
 hh_id_key_stb_id_dict={}
 with gzip.open(obj_r, 'rb') as f:
     for idx, line_byte in enumerate(f):
         hh_id_key_stb_id_dict={}
         if idx ==0: continue;
 #         if not (b'CPX022' in line_byte): continue;
         line_obj=io.BytesIO(line_byte)
 #         print("idx=", idx, type(line_obj.getvalue().decode("utf-8")),"\n",  line_obj.getvalue().decode("utf-8") )
         #print("\nidx=", idx)
         line_str=line_obj.getvalue().decode("utf-8")
         line_strlist_aftersplit=line_str.split('|')
         STB_id=line_str.split('|')[3]
         HH_id=line_str.split('|')[2]         
 #         stb_model=line_str.split('|')[4] 
 #         dvr_flg=line_str.split('|')[9]
         #print("HH_id=",HH_id,"STB_id=",STB_id)           
         hh_id_key_stb_id_dict['hh_id']=HH_id
         hh_id_key_stb_id_dict['stb_id']=STB_id
 #         hh_id_key_stb_id_dict['stb_model']=stb_model
 #         hh_id_key_stb_id_dict['dvr_flg']=dvr_flg        
         hh_id_key_stb_id_list.append(hh_id_key_stb_id_dict)
 #        print("hh_id_key_stb_id_dict", hh_id_key_stb_id_dict)
 #         if idx>1000 : break;
         if idx%100000==0 : print("idx=",idx);
 # print(Counter(chain.from_iterable(dic.keys() for dic in hh_id_key_stb_id_list)))
 cox_raw_device = pd.DataFrame(hh_id_key_stb_id_list)
 return cox_raw_device